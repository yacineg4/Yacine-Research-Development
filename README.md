# Yacine-Research-Development
Repository for testing and development of new functionalities
## Technologies to use in GRAD4:
- React + Bootstrap + React-Router (Frontend)
- Django + Django REST API (Backend)

## Technologies pending usage in GRAD4:
- Redux :Used for creating a unique state for many components, not needed for the moment. When managing the states of the components will become too complicated we will refactor the app and use it then.
- Webpack4 configuration of React from scratch : We don't need to use this for now. create-react-app will work for the start, when we need to tweak the configuration we will eject the webpack config and handle it manually from there. Setting the basic stuff with Webpack is a bit time consuming.
- SASS and preprocessing based are not that useful with react as the composition paradigm is already simplifying greatly css code reuse.

## Potential Technologies
- GraphQL
- Flow or Typescript

## Technologies currently in review:
- Flexbox based CSS
- Will need to test two repository architecture or a monorepository architecture with the team. Tutorial for two repos setting: https://medium.com/@dakota.lillie/django-react-jwt-authentication-5015ee00ef9a and for one repo : https://www.valentinog.com/blog/drf/
