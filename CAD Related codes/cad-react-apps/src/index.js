import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CadViewerApp from './Components/App';

ReactDOM.render(<CadViewerApp />, document.getElementById('root'));
