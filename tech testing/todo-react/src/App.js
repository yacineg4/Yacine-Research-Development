import React, { Component } from 'react';
import './App.css';

import Header from './Components/Header/Header';
import Todo from './Components/Content/Todo';
import Footer from './Components/Footer/Footer';

class AppV1 extends Component {
    constructor(props){
      super(props);
      this.state ={
        styling: {},
      }

      this.updateColor = this.updateColor.bind(this);
    }

    componentDidMount() {
        this.timer = setInterval(this.updateColor, 1000);
    }

    componentWillUnmount(){
      clearInterval(this.timer);
    }

    updateColor(){
      const date = new Date()
      const seconds = date.getSeconds()

      // To test we can modulate the color depending on the seconds
      let red_ratio = (seconds/60)*100;
      let styling = {
        color: "rgb(" + red_ratio + "%,10%,100%)",
      }

      this.setState({styling: styling});
      console.log(styling);

    }

    render() {
      return (
        <div className="App">
          <Header />
          <h1 style={this.state.styling}> TODO LIST </h1>
          <Todo />
          <Footer />
        </div>
      );
    }
}

export default AppV1;
