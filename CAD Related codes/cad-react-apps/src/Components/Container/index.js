import React, { Component } from 'react';
import './Container.css';

class ImageContainer extends Component {
  render() {
    const altDetail = "Image of a CAD in multiple angles.";
    return (
      <div className="imageContainer">
         <img src={this.props.image} alt={altDetail}/>
      </div>
    );
  }
}

export default ImageContainer;
