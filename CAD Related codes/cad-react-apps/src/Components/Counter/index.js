import React, { Component } from 'react';
import './Dot.css';
class DotCounter extends Component {

  createDots(number_dots, type, selected_index){
    let dots = [];
    for(let i = 0; i < number_dots; i++){
      if(selected_index === i){
        dots.push(<Dot type={type}
                      dot_style={"filled"}
                      key={i}/>);
      }else{
        dots.push(<Dot type={type}
                       dot_style={"hollow"}
                       key={i}/>);
      }
    }
    return dots;
  }

  render() {
    return (
      <div className="dotCounter">
        {this.createDots(this.props.number_dots,this.props.type,this.props.selected_index)}
      </div>
    );
  }
}

// Helper Components
class Dot extends Component{

  render(){
    let hollow_dot = <path d="M256,0C114.618,0,0,114.618,0,256s114.618,256,256,256s256-114.618,256-256S397.382,0,256,0z M256,469.333    c-117.818,0-213.333-95.515-213.333-213.333S138.182,42.667,256,42.667S469.333,138.182,469.333,256S373.818,469.333,256,469.333z"/>;
    let filled_dot = <path d="M256,0C114.837,0,0,114.837,0,256s114.837,256,256,256s256-114.837,256-256S397.163,0,256,0z"/>;
    let svg = hollow_dot;
    let type = "mediumDot";

    // Select the style of the dot
    if(this.props.type === "small"){
        type = "smallDot";
    }else if(this.props.type === "big"){
        type = "bigDot";
    }

    if(this.props.dot_style === "filled"){
      svg = filled_dot;
    }



    return (
      <div className={type}>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 512 512" >
        		{svg}
        </svg>
      </div>
    );
  }
}


export default DotCounter;
