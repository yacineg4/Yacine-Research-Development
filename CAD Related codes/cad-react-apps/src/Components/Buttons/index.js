import React, { Component } from 'react';
import './Arrow.css';

class ArrowButton extends Component {
  render() {
    let left_arrow = <g transform="matrix(-0.26458333,0,0,0.26458333,149.88752,107.04704)" ><path d="M 61.792,2.588 C 64.771,0.864 68.105,0 71.444,0 c 3.33,0 6.663,0.864 9.655,2.588 l 230.116,167.2 c 5.963,3.445 9.656,9.823 9.656,16.719 0,6.895 -3.683,13.272 -9.656,16.713 L 81.099,370.427 c -5.972,3.441 -13.334,3.441 -19.302,0 -5.973,-3.453 -9.66,-9.833 -9.66,-16.724 V 19.305 c 0,-6.892 3.681,-13.269 9.655,-16.717 z"/></g>;
    let right_arrow = <g transform="matrix(0.26458333,0,0,0.26458333,51.195821,107.04704)"> <path d="M 61.792,2.588 C 64.771,0.864 68.105,0 71.444,0 c 3.33,0 6.663,0.864 9.655,2.588 l 230.116,167.2 c 5.963,3.445 9.656,9.823 9.656,16.719 0,6.895 -3.683,13.272 -9.656,16.713 L 81.099,370.427 c -5.972,3.441 -13.334,3.441 -19.302,0 -5.973,-3.453 -9.66,-9.833 -9.66,-16.724 V 19.305 c 0,-6.892 3.681,-13.269 9.655,-16.717 z"/></g>
    let svg;
    let direction;
    let increment;
    if(this.props.direction === "left"){
      svg = left_arrow;
      direction = "leftArrow";
      increment = -1;
    }else{
      svg = right_arrow;
      direction = "rightArrow";
      increment = 1;
    }

    let type = "mediumArrow";
    // Select the style of the dot
    if(this.props.type === "small"){
        type = "smallArrow";
    }else if(this.props.type === "big"){
        type = "bigArrow";
    }


    return (
      <div className={`${type} ${direction}`} onClick={() => this.props.change_image_index(increment)}>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  width="100%" viewBox="0 0 220 250" >
            {svg}
        </svg>
      </div>
    );
  }
}

export default ArrowButton;
