import React, { Component } from 'react';
import './CadViewerApp.css';
import ArrowButton from './../Buttons';
import DotCounter from './../Counter';
import ImageContainer from './../Container';

class CadViewerApp extends Component {
  constructor(props){
    super(props);
    this.state = {
      images: [],
      selected_image_index: 0,
    };
    // TODO: Add preference here

    // Binding
    this.change_image_index = this.change_image_index.bind(this);
  }

  componentDidMount() {
    let images = [];

    // Fetch the images from the server
    images.push("https://s3.ca-central-1.amazonaws.com/grad4-static-data/static/image/team/fr%C3%A9d%C3%A9rik_plourde.webp");
    images.push("https://s3.ca-central-1.amazonaws.com/grad4-static-data/static/image/team/f%C3%A9lix_b%C3%A9lisle.webp");
    images.push("https://s3.ca-central-1.amazonaws.com/grad4-static-data/static/image/team/nicolas_gauthier.webp");
    images.push("https://s3.ca-central-1.amazonaws.com/grad4-static-data/static/image/team/yacine_mahdid.webp");

    this.setState({images: images});
  }

componentWillUnmount() {

}

change_image_index(increment){
  let new_index = this.state.selected_image_index + increment;
  if(new_index < 0){
    this.setState({selected_image_index: this.state.images.length-1});
  }else if(new_index === this.state.images.length){
    this.setState({selected_image_index: 0});
  }else{
    this.setState({selected_image_index: new_index});
  }
}

render() {
  const number_dots = this.state.images.length;
  const dot_type = "medium";
  const arrow_type = "big";

  return (
    <div className="App">
      <div className="cadViewer">
        <p>CAD VIEWER</p>
        <ArrowButton direction={"left"}
                     type={arrow_type}
                     change_image_index={this.change_image_index}
        />
        <ImageContainer image={this.state.images[this.state.selected_image_index]}/>
        <ArrowButton direction={"right"}
                     type={arrow_type}
                     change_image_index={this.change_image_index}
        />
        <DotCounter number_dots={number_dots}
                    type={dot_type}
                    selected_index={this.state.selected_image_index}
        />
      </div>
    </div>
  );
}
}

export default CadViewerApp;
