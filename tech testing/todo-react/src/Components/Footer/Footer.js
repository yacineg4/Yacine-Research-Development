import React from 'react';
import './Footer.css';

function Footer() {
  return (
    <div className="footer">
      Made by Yacine from GRAD4 (
      <a href="https://github.com/grad4/Yacine-Research-Development/tree/master/tech%20testing/todo-react">
        <strong>Github</strong>
      </a>) .
    </div>
  );
}

export default Footer;
