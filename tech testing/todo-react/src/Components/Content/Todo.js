import React from 'react';
import './Todo.css';

function TodoItem(){
  return (
    <div className="todoItem">
      <label> Dummy Text <input type="checkbox" name="TODO"/></label>
    </div>
  );
}

function Todo() {
  return (
    <div className="todo">
      <TodoItem/>
      <TodoItem/>
      <TodoItem/>
    </div>
  );
}

export default Todo;
