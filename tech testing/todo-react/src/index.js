import React from 'react';
import ReactDOM from 'react-dom';
import AppV1 from './App';

// CSS import
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

ReactDOM.render(<AppV1 />, document.getElementById('root'));
